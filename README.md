# Midships K8s Solid
<p>Solid is a project that uses Web standards to let people control their data, and choose the applications and services to use with it. More info can be found at https://solidproject.org.</p>

<p>Using the community project located at https://github.com/solid/solidproject.org as a base, we at Midships have ported the Solid Server solution to Kubernetes enabling individual to easily run their own Solid Server hosted on a Kubernetes cluster with their own bespoke domain name.</p>

</p>
<b>Below are the pre-requisites:</b><br>
<ul>
  <li>Have access to a domain name. For instance example.com.</li>
  <li>Have the ability to update and add a TXT record to the above domain name. For instance be able to add yourname.example.com.</li>
  <li>A Kubernetes cluster</li>
  <li>kubectl installed and connected to your Kubernetes cluster</li>
  <li>Helm 3 installed and connected to your Kubernetes cluster</li>
  <li>Git installed</li>
</ul>

<p>
<b>Steps to run solution from the git repo</b><br>
<ol>
  <li>Git clone the below public repository to get the required helm files <br>https://gitlab.com/midships-community/midships-k8s-solid.git</li>
  <li>In a terminal navigate to the folder containing the cloned repository.</li>
  <li>Run the below helm command substituting the variables contents as required. All variables are REQUIRED. <br>
<i>helm install \<br>
--kubeconfig "<path to you K8s cluster config>" \<br>
--set midships_k8s_solid.image="<path to the midships-k8s-solid image in your container registry>" \<br>
--set midships_k8s_solid.pod_name="midships-k8s-solid" \<br>
--set midships_k8s_solid.service_name="midships-k8s-solid" \<br>
--set midships_k8s_solid.domain="example.com" \<br>
--set midships_k8s_solid.uname="yourname_no_spaces" \<br>
--set midships_k8s_solid.port="8443" \<br>
--set midships_k8s_solid.server_summary="My Solid Server" \<br>
--namespace default \<br>
midships-k8s-solid helm/</i></li>
  <li>Execute the following command and verify the service external IP created for access the midships-solid-server pod. <br>
<i>kubectl get svc</i></li>
  <li>Take note of the IP address when it is allocated. In the case where an External address is a domain, you can <i>ping</i> the domain know the IP address.</li>
  <li>Check with your K8s cluster Administrator that the External address provided is accessible over the internet. Without this you won't be able to access your Solid Server over the internet.
  <li>Update the A record for your <i>example.com</i> domain to the IP address</li>
  <li>Add a new A record for <i>username.example.com</i> and set to the IP address</li>
  <li>Navigate to your domain in a browser with HTTPS. For example <i>https://example.com</i> to access your Solid server.<br>
    <b>Note:</b> In some cases you will see a screen complaining about missing ACLs. All you need to do is restart(delete) the Pod.</li>
  <li>Click the <i>Register</i> button to begin the create an account process</li>
  <li>Enter the <i>username</i> as the same name provided in the helm command for <i>uname</i>. For instance, <i>yourname_no_spaces</i>. The rest of the fields can be filled to your discretion.</li>
  <li>Click the <i>Register</i> button to create your account</li>
  <li>Navigate to your created solid space in a browser using <i>port</i> from your helm command and the URL <i>https://yourname_no_spaces.example.com:8443</i> substituting the values with your actual domain and username</li>
  <li>Click the <i>Login</i> button</li>
  <li>On the resulting window, click the button with your username and domain. You will be navigated to a page requesting your <i>username</i> and <i>password</i>/li>
  <li>Enter your <i>username</i> and <i>password</i> and click the <i>Login</i> button to continue</li>
  <li>You are now logged in. Click the Profile/Card URL to access your Solid</li>
</ol>
