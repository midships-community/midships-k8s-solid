FROM node:lts

# build
ENV APP_HOME="/usr/src/app"

# Setup users and folders
# -----------------------
RUN apt install sed \
 && groupmod -g 1000 node \
 && useradd -m -d ${APP_HOME} -s /bin/nologin solid -u 10001 -g 1000 \
 && id -u solid \
 && id -g solid

USER solid
RUN mkdir -p ${APP_HOME}
WORKDIR ${APP_HOME}
COPY package.json ${APP_HOME}/
RUN npm install
COPY . ${APP_HOME}

# start
EXPOSE 8443
COPY config.json config.json
COPY init.sh init.sh

USER root
RUN chmod +x ${APP_HOME}/init.sh \
 && ls -ltr ${APP_HOME}

RUN rm -f /usr/src/app/node_modules/ssh2/test/fixtures/id_dsa /usr/src/app/node_modules/ssh2/test/fixtures/ssh_host_dsa_key /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_dsa_enc /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_dsa /usr/src/app/node_modules/ssh2/test/fixtures/ssh_host_ecdsa_key /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_ecdsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_ecdsa_enc /usr/src/app/node_modules/public-encrypt/test/ec.priv /usr/src/app/node_modules/ssh2/test/fixtures/id_ecdsa /usr/src/app/node_modules/public-encrypt/test/pass.1024.priv /usr/src/app/node_modules/public-encrypt/test/ec.pass.priv /usr/src/app/node_modules/ssh2/test/fixtures/openssh_new_rsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_rsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_dsa_enc_gcm /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_rsa_enc_gcm /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_dsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_ecdsa_enc_gcm /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_rsa_enc /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_ecdsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_dsa_enc /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_ecdsa_enc /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_new_ed25519 /usr/src/app/test/keys/user1-key.pem /usr/src/app/node_modules/public-encrypt/test/1024.priv /usr/src/app/node_modules/ssh2/test/fixtures/bad_rsa_private_key /usr/src/app/node_modules/@solid/oidc-op/test/lib/private.pem /usr/src/app/node_modules/public-encrypt/test/rsa.pass.priv /usr/src/app/node_modules/public-encrypt/test/test_key.pem /usr/src/app/node_modules/public-encrypt/test/test_rsa_privkey.pem /usr/src/app/test/keys/key.pem /usr/src/app/node_modules/ssh2/test/fixtures/id_rsa /usr/src/app/node_modules/agent-base/test/ssl-cert-snakeoil.key /usr/src/app/node_modules/proxy-agent/test/ssl-cert-snakeoil.key /usr/src/app/node_modules/socks-proxy-agent/node_modules/agent-base/test/ssl-cert-snakeoil.key /usr/src/app/node_modules/socks-proxy-agent/test/ssl-cert-snakeoil.key /usr/src/app/node_modules/http-proxy-agent/test/ssl-cert-snakeoil.key /usr/src/app/node_modules/pac-proxy-agent/test/ssl-cert-snakeoil.key /usr/src/app/test/keys/user2-key.pem /usr/src/app/node_modules/ssh2/test/fixtures/ssh_host_rsa_key /usr/src/app/node_modules/get-uri/test/server.key /usr/src/app/node_modules/public-encrypt/test/rsa.2028.priv /usr/src/app/node_modules/ssh2/test/fixtures/id_rsa_enc /usr/src/app/node_modules/public-encrypt/test/test_rsa_privkey_encrypted.pem /usr/src/app/test/keys/client-key.pem /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_rsa /usr/src/app/node_modules/ssh2-streams/test/fixtures/openssh_old_rsa_enc /usr/src/app/node_modules/public-encrypt/test/rsa.1024.priv

CMD /usr/src/app/init.sh
