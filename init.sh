#!/bin/bash

sed -i "s+!!UNAME!!+${UNAME}+g" /usr/src/app/config.json
sed -i "s+!!DOMAIN!!+${DOMAIN}+g" /usr/src/app/config.json
sed -i "s+!!PORT!!+${PORT}+g" /usr/src/app/config.json
sed -i "s+!!SERVER_SUMMARY!!+${SERVER_SUMMARY}+g" /usr/src/app/config.json

echo "-> Updated config.json"
echo ""
cat /usr/src/app/config.json
echo ""

openssl req \
  -new \
  -newkey rsa:4096 \
  -days 1095 \
  -nodes \
  -x509 \
  -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
  -keyout /opt/solid/privkey.pem \
  -out /opt/solid/fullchain.pem

echo "-> Starting your Solid server"
echo ""
npm run solid start --no-reject-unauthorized
